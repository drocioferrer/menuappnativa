package com.example.nav_drawer;

import java.util.HashMap;

public class Cliente {
    public String id;
    public String contraseña;
    public String nombre;
    public String email;
    public HashMap<String, Boolean> carrito;

    public Cliente(){}

    public Cliente(String id, String contraseña, String nombre, String email){
        this.id = id;
        this.contraseña = contraseña;
        this.nombre = nombre;
        this.email = email;
        this.carrito = new HashMap<>();
    }
}
