package com.example.nav_drawer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Login extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference myRef;
    ArrayList<Cliente> listaClientes;
    EditText edtCodigo;
    Button btnIngresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        listaClientes = new ArrayList<>();
        edtCodigo = findViewById(R.id.edtContraseña);
        btnIngresar = findViewById(R.id.btnIngresar);

        myRef.child("clientes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChildren()){
                    listaClientes.clear();
                    for(DataSnapshot cliente:dataSnapshot.getChildren()){
                        listaClientes.add(cliente.getValue(Cliente.class));
                    }
                } else {
                    Cliente cliente = new Cliente(myRef.push().getKey(), "C001", "Maria", "maria.perez@gmail.com");
                    listaClientes.add(cliente);
                    myRef.child("clientes").child(cliente.id).setValue(cliente);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String contraseña = edtCodigo.getText().toString();
                boolean correcto = false;

                for(Cliente cliente:listaClientes) {
                    if(contraseña.equalsIgnoreCase(cliente.contraseña)){
                        correcto = true;
                        Intent intent = new Intent(Login.this, MainActivity2.class);
                        intent.putExtra("cliente", cliente.id);
                        startActivity(intent);
                    }
                }

                if(!correcto){
                    Toast.makeText(Login.this, "Cliente no existe", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}