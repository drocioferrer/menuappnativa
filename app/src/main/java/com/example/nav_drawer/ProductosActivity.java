package com.example.nav_drawer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ProductosActivity extends AppCompatActivity {

    TextView tvNombre;
    Button btnCarrito;
    ListView lvProductos;
    Cliente cliente;

    FirebaseDatabase database;
    DatabaseReference myRef;

    ArrayList<Producto> listaProductos;
    ArrayAdapter<Producto> adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);

        String id = getIntent().getExtras().getString("cliente");
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        tvNombre = findViewById(R.id.tvNombre);
        btnCarrito = findViewById(R.id.btnCarrito);
        lvProductos = findViewById(R.id.lvProductos);

        listaProductos = new ArrayList<>();

        adaptador = new ArrayAdapter<Producto>(ProductosActivity.this, android.R.layout.simple_list_item_1, listaProductos);

        lvProductos.setAdapter(adaptador);

        /*lvProductos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ProductosActivity.this, DetalleActivity.class);
                intent.putExtra("producto", listaProductos.get(position).id);
                intent.putExtra("cliente", cliente.id);
                startActivity(intent);
            }
        });*/

        myRef.child("productos").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChildren()) {
                    listaProductos.clear();
                    for (DataSnapshot dato: dataSnapshot.getChildren()) {
                        Producto producto = dato.getValue(Producto.class);
                        listaProductos.add(producto);
                        adaptador.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        myRef.child("clientes").child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                cliente = dataSnapshot.getValue(Cliente.class);
                tvNombre.setText(cliente.nombre);

                if(cliente.carrito != null) {
                    btnCarrito.setText(getString(R.string.ver_carrito) + " (" + cliente.carrito.size() + ")");
                } else {
                    btnCarrito.setText(getString(R.string.ver_carrito));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        /*btnCarrito.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductosActivity.this, CarritoActivity.class);
                intent.putExtra("cliente", cliente.id);
                startActivity(intent);
            }
        });*/
    }
}